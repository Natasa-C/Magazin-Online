// Dependencies
import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

// Components

// Shared
import styles from "./Card.module.css";

export default function ListModal({ modal, setModal, listName, listInfo }) {
  function getListItems() {
    let items = [];

    for (let i = 0; i < listInfo.length; i++) {
      items.push(
        <p key={i}>
          {i + 1}. {listInfo[i]}
        </p>
      );
    }

    return items;
  }

  function toggle() {
    setModal(!modal);
  }

  return (
    <div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        backdrop="static"
        className={styles.modal}
        returnFocusAfterClose={false}
      >
        <ModalHeader toggle={toggle}>{listName}</ModalHeader>
        <ModalBody>
          {getListItems()}
          <ModalFooter>
            <Button color="secondary" onClick={toggle}>
              Close
            </Button>
          </ModalFooter>
        </ModalBody>
      </Modal>
    </div>
  );
}
