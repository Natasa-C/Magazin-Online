// Dependencies
import React, { useState, useEffect, useRef } from "react";
import { Button } from "reactstrap";
import { Form, Table } from "react-bootstrap";
import { firstBy } from "thenby";

// Components
import Card from "./Card";
import AddModal from "./AddModal";
import ListModal from "./ListModal";

// Shared
import styles from "./MainPage.module.css";

export default function MainPage() {
  const [productsList, setProductsList] = useState(null);
  const [shoppingLists, setShoppingLists] = useState(null);
  const [cardContent, setCardContent] = useState(null);

  const [isPending, setIsPending] = useState(true);
  const [isPendingShopping, setIsPendingShopping] = useState(true);
  const [error, setError] = useState(null);

  const [modalAdd, setModalAdd] = useState(false);
  const [modalShoppingList, setModalShoppingList] = useState(false);
  const [reloadData, setReloadData] = useState(true);
  const searchCriteriaRef = useRef("");

  const [shoppingListName, setShoppingListName] = useState("Produsele mele");

  let headers = [
    "Nume",
    "Pret",
    "Cantitate",
    "Descriere",
    "Comentarii",
    "Rating",
  ];

  let jsonProp = [
    "name",
    "price",
    "quantity",
    "description",
    "comments",
    "rating",
  ];
  const [sortOptions, setSortOptions] = useState(Array(6).fill("⬍"));

  useEffect(() => {
    if (reloadData === true) {
      setCardContent(null);
      setProductsList(null);
      setShoppingLists(null);
      setError(null);

      fetch("http://localhost:8000/products")
        .then((res) => {
          if (!res.ok) {
            throw Error("could not fetch data for resource");
          }
          return res.json();
        })
        .then((data) => {
          setIsPending(false);

          setCardContent(data);
          setProductsList(data);
        })
        .catch((err) => {
          setIsPending(false);
          setError(err.message);
        });

      fetch("http://localhost:8000/lists")
        .then((res) => {
          if (!res.ok) {
            throw Error("could not fetch data for resource");
          }
          return res.json();
        })
        .then((data) => {
          setIsPendingShopping(false);

          setShoppingLists(data);
        })
        .catch((err) => {
          setIsPendingShopping(false);
          setError(err.message);
        });

      setReloadData(false);
    }
  }, [reloadData]);

  useEffect(() => {
    if (!cardContent) return;

    // reorder content after each sort selection
    function getSortOrder(index) {
      if (sortOptions[index] === "⬍") return "";
      if (sortOptions[index] === "▲") return "asc";
      if (sortOptions[index] === "▼") return "desc";
    }

    function getSortName(index) {
      if (sortOptions[index] === "⬍") return "";
      if (sortOptions[index] === "▲" || sortOptions[index] === "▼")
        return jsonProp[index].toLocaleLowerCase();
    }

    // sort columns from left to right by the specified properties
    setCardContent((prevSort) => {
      // deep copy old cards content
      let copy = JSON.parse(JSON.stringify(prevSort));
      copy.sort(
        firstBy(getSortName(0), getSortOrder(0))
          .thenBy(getSortName(1), getSortOrder(1))
          .thenBy(getSortName(2), getSortOrder(2))
          .thenBy(getSortName(3), getSortOrder(3))
          .thenBy(getSortName(4), getSortOrder(4))
          .thenBy(getSortName(5), getSortOrder(5))
      );
      return copy;
    });
  }, [sortOptions]);

  function toggleAdd() {
    setModalAdd(!modalAdd);
  }

  function toggleShoppingList() {
    setModalShoppingList(!modalShoppingList);
  }

  function deleteProduct(index) {
    fetch(`http://localhost:8000/products/${index}`, {
      method: "DELETE",
    })
      .then(() => {
        setReloadData(true);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  function generateCards() {
    let cards = [];

    for (let i = 0; i < cardContent.length; i++) {
      cards.push(
        <Card
          key={i}
          cardInfo={cardContent[i]}
          deleteProduct={deleteProduct}
          setReloadData={setReloadData}
          inShoppingList={checkProductInSoppingList(i)}
        />
      );
    }

    return cards;
  }

  function handleSearch(e) {
    e.preventDefault();
    searchCriteriaRef.current = e.target.value;
    console.log(searchCriteriaRef.current);

    // search after the given criteria
    let filtered = productsList.filter(
      (product) =>
        product.name
          .toLocaleLowerCase()
          .indexOf(searchCriteriaRef.current.toLocaleLowerCase()) !== -1
    );

    setCardContent(filtered);
  }

  function getHeader() {
    let tabel = [];

    for (let index = 0; index < headers.length; index++) {
      tabel.push(
        <th key={`th${index}`} onClick={(e) => handleClick(index)}>
          <div className={styles.headItem}>
            {headers[index]}
            <div>{sortOptions[index] === null ? null : sortOptions[index]}</div>
          </div>
        </th>
      );
    }

    return tabel;
  }

  function handleClick(index) {
    if (sortOptions[index] === "⬍") {
      setSortOptions((prevSort) => {
        let copy = JSON.parse(JSON.stringify(prevSort));
        copy[index] = "▲";
        return copy;
      });
    } else if (sortOptions[index] === "▲") {
      setSortOptions((prevSort) => {
        let copy = JSON.parse(JSON.stringify(prevSort));
        copy[index] = "▼";
        return copy;
      });
    } else if (sortOptions[index] === "▼") {
      setSortOptions((prevSort) => {
        let copy = JSON.parse(JSON.stringify(prevSort));
        copy[index] = "⬍";
        return copy;
      });
    }
  }

  function checkProductInSoppingList(index) {
    for (let i = 0; i < shoppingLists.length; i++) {
      if (shoppingLists[i].id.localeCompare(productsList[index].id) === 0)
        return true;
    }

    return false;
  }

  return (
    <React.Fragment>
      <div className={styles.searchArea}>
        <Form.Group controlId="searchId">
          <Form.Control
            ref={searchCriteriaRef}
            onChange={handleSearch}
            className={styles.searchInput}
            placeholder="Cauta dupa nume..."
          />
        </Form.Group>

        <Button
          color="info"
          className={styles.shoppingList}
          onClick={toggleShoppingList}
        >
          Vezi lista de produse
        </Button>

        <Button
          color="success"
          className={styles.addProductButton}
          onClick={toggleAdd}
        >
          Adauga produs
        </Button>
      </div>

      <Table responsive className={styles.tableContent}>
        <thead className={styles.thead}>
          <tr key="trHead">{getHeader()}</tr>
        </thead>
      </Table>

      {error && <div>{error}</div>}
      {isPending && isPendingShopping && <div>Loading...</div>}
      {cardContent && shoppingLists && productsList && (
        <div className={styles.cardsContainer}>{generateCards()}</div>
      )}

      <AddModal
        setModal={setModalAdd}
        modal={modalAdd}
        setReloadData={setReloadData}
      />

      <ListModal
        setModal={setModalShoppingList}
        modal={modalShoppingList}
        listName={shoppingListName}
        listInfo={
          cardContent && shoppingLists && productsList
            ? cardContent
                .filter((_, index) => checkProductInSoppingList(index))
                .map(
                  (product) =>
                    `${product.name}, pret: ${product.price}, cantitate: ${product.quantity}`
                )
            : []
        }
      />
    </React.Fragment>
  );
}
