// Dependencies
import React from "react";

// Components

// Shared
import styles from "./Header.module.css";

export default function Header() {
  return (
    <div className={styles.header}>
      <div className={styles.name}>Magazin Online</div>
    </div>
  );
}
