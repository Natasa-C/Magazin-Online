// Dependencies
import React, { useState, useRef } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, Col } from "react-bootstrap";

// Components

// Shared
import styles from "./Card.module.css";

export default function EditModal({ modal, setModal, setReloadData, info }) {
  const nameRef = useRef(info.name);
  const priceRef = useRef(info.price);
  const quantityRef = useRef(info.quantity);
  const descriptionRef = useRef(info.description);
  const commentsRef = useRef(info.comments);
  const ratingRef = useRef(info.rating);
  const [validated, setValidated] = useState(false);

  function toggle() {
    setModal(!modal);
    setValidated(false);
  }

  function handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    if (!validated) setValidated(true);

    if (form.checkValidity() === true) {
      fetch(`http://localhost:8000/products/${info.id}`, {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name: nameRef.current.value,
          price: parseFloat(priceRef.current.value),
          quantity: quantityRef.current.value,
          description: descriptionRef.current.value,
          comments: commentsRef.current.value,
          rating: ratingRef.current.value,
        }),
      })
        .then(() => {
          setReloadData(true);
        })
        .catch((err) => {
          console.log(err.message);
        });

      toggle();
    }
  }

  return (
    <div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        backdrop="static"
        className={styles.modal}
        returnFocusAfterClose={false}
      >
        <ModalHeader toggle={toggle}>Edit product</ModalHeader>
        <ModalBody>
          <Form
            noValidate
            validated={validated}
            onSubmit={handleSubmit}
            autoComplete="off"
          >
            <Form.Group as={Col} controlId="validationName">
              <Form.Label>Nume</Form.Label>
              <Form.Control
                contentEditable
                ref={nameRef}
                required
                type="input"
                defaultValue={info.name}
                maxLength="25"
                minLength="2"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be between 2 and 25 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationPrice">
              <Form.Label>Pret</Form.Label>
              <Form.Control
                ref={priceRef}
                required
                type="number"
                defaultValue={info.price}
                precision={3}
                step={0.01}
                min={0}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Please introduce a value grater then or equal to zero, with a
                maximum of 2 integers after comma.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationQuantity">
              <Form.Label>Cantitate</Form.Label>
              <Form.Control
                ref={quantityRef}
                type="number"
                defaultValue={info.quantity}
                precision={3}
                step={0.01}
                min={0}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Please introduce a value grater then or equal to zero, with a
                maximum of 2 integers after comma.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationDescription">
              <Form.Label>Descriere</Form.Label>
              <Form.Control
                ref={descriptionRef}
                as="textarea"
                defaultValue={info.description}
                maxLength="250"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be at most 250 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationComments">
              <Form.Label>Comentarii</Form.Label>
              <Form.Control
                ref={commentsRef}
                as="textarea"
                defaultValue={info.comments}
                maxLength="250"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be at most 250 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="exampleForm.ControlSelectRating">
              <Form.Label>Rating</Form.Label>
              <Form.Control
                as="select"
                ref={ratingRef}
                defaultValue={info.rating}
              >
                <option value="" disabled>
                  Choose...
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please select a rating.
              </Form.Control.Feedback>
            </Form.Group>

            <ModalFooter>
              <Button color="primary" type="submit">
                Edit
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}
