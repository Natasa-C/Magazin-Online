// Dependencies
import React, { useState, useRef } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, Col } from "react-bootstrap";
import { v4 as uuidv4 } from 'uuid';

// Components

// Shared
import styles from "./Card.module.css";

export default function AddModal({ modal, setModal, setReloadData }) {
  const nameRef = useRef();
  const priceRef = useRef();
  const quantityRef = useRef(0);
  const descriptionRef = useRef("");
  const commentsRef = useRef("");
  const ratingRef = useRef("-");

  const [validated, setValidated] = useState(false);

  function toggle() {
    setModal(!modal);
    setValidated(false);
  }

  function handleSubmit(event) {
    event.preventDefault();
    event.stopPropagation();

    const form = event.currentTarget;
    if (!validated) setValidated(true);

    if (form.checkValidity() === true) {
      fetch("http://localhost:8000/products", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id: uuidv4(),
          name: nameRef.current.value,
          price: priceRef.current.value,
          quantity: quantityRef.current.value,
          description: descriptionRef.current.value,
          comments: commentsRef.current.value,
          rating: ratingRef.current.value,
        }),
      })
        .then(() => {
          setReloadData(true)
        })
        .catch((err) => {
          console.log(err.message);
        });

      toggle();
    }
  }

  return (
    <div>
      <Modal
        isOpen={modal}
        toggle={toggle}
        backdrop="static"
        className={styles.modal}
        returnFocusAfterClose={false}
      >
        <ModalHeader toggle={toggle}>Add product</ModalHeader>
        <ModalBody>
          <Form
            noValidate
            validated={validated}
            onSubmit={handleSubmit}
            autoComplete="off"
          >
            <Form.Group as={Col} controlId="validationName">
              <Form.Label>Nume</Form.Label>
              <Form.Control
                ref={nameRef}
                required
                type="input"
                placeholder="adauga nume"
                maxLength="25"
                minLength="2"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be between 2 and 25 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationPrice">
              <Form.Label>Pret</Form.Label>
              <Form.Control
                ref={priceRef}
                required
                type="number"
                placeholder="adauga pret"
                precision={3}
                step={0.01}
                min={0}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Please introduce a value grater then or equal to zero, with a
                maximum of 2 integers after comma.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationQuantity">
              <Form.Label>Cantitate</Form.Label>
              <Form.Control
                ref={quantityRef}
                type="number"
                placeholder="adauga cantitate"
                precision={3}
                step={0.01}
                min={0}
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                Please introduce a value grater then or equal to zero, with a
                maximum of 2 integers after comma.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationDescription">
              <Form.Label>Descriere</Form.Label>
              <Form.Control
                ref={descriptionRef}
                as="textarea"
                placeholder="adauga descriere"
                maxLength="250"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be at most 250 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="validationComments">
              <Form.Label>Comentarii</Form.Label>
              <Form.Control
                ref={commentsRef}
                as="textarea"
                placeholder="adauga comentarii"
                maxLength="250"
              />
              <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
              <Form.Control.Feedback type="invalid">
                The name should be at most 250 chracters.
              </Form.Control.Feedback>
            </Form.Group>

            <Form.Group as={Col} controlId="exampleForm.ControlSelectRating">
              <Form.Label>Rating</Form.Label>
              <Form.Control as="select" ref={ratingRef} defaultValue="">
                <option value="" disabled>
                  Choose...
                </option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please select a rating.
              </Form.Control.Feedback>
            </Form.Group>

            <ModalFooter>
              <Button color="primary" type="submit">
                Add
              </Button>
              <Button color="secondary" onClick={toggle}>
                Cancel
              </Button>
            </ModalFooter>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  );
}
