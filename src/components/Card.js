// Dependencies
import React, { useState } from "react";

// Components
import EditModal from "./EditModal";

// Shared
import styles from "./Card.module.css";
import produce from "../images/produce.jpg";
import editIcon from "../images/draw.png";
import deleteIcon from "../images/delete.png";
import emptyStar from "../images/emptyStar.png";
import fullStar from "../images/fullStar.png";

export default function Card({
  cardInfo,
  deleteProduct,
  setReloadData,
  inShoppingList,
}) {
  const [modalEdit, setModalEdit] = useState(false);

  function toggleEdit() {
    setModalEdit(!modalEdit);
  }

  function handleDelete(e) {
    e.preventDefault();
    deleteProduct(cardInfo.id);
  }

  function handleEdit(e) {
    e.preventDefault();
    toggleEdit();
  }

  function addInShoppingList() {
    fetch("http://localhost:8000/lists", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id: cardInfo.id,
      }),
    })
      .then(() => {
        setReloadData(true);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  function removeFromShoppingList() {
    fetch(`http://localhost:8000/lists/${cardInfo.id}`, {
      method: "DELETE",
    })
      .then(() => {
        setReloadData(true);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  return (
    <div className={`card ${styles.cardItem} `}>
      <img className={`${styles.cardImage}`} src={produce} alt="product" />
      <div className={styles.details}>
        <p>Nume: {cardInfo.name}</p>
        <p>Pret: {cardInfo.price}</p>
        <p>Cantitate: {cardInfo.quantity}</p>
        <p>Descriere: {cardInfo.description}</p>
        <p>Comentarii: {cardInfo.comments}</p>
        <p>Rating: {cardInfo.rating}</p>
      </div>
      <div className="card-footer">
        {inShoppingList ? (
          <img
            className={`${styles.star}`}
            src={fullStar}
            onClick={removeFromShoppingList}
            alt="fullStar icon"
          />
        ) : (
          <img
            className={`${styles.star}`}
            src={emptyStar}
            onClick={addInShoppingList}
            alt="emptyStar icon"
          />
        )}

        <img
          className={`${styles.icon}`}
          src={editIcon}
          onClick={handleEdit}
          alt="edit icon"
        />
        <img
          className={`${styles.icon}`}
          src={deleteIcon}
          onClick={handleDelete}
          alt="delete icon"
        />
      </div>
      <EditModal
        setModal={setModalEdit}
        modal={modalEdit}
        setReloadData={setReloadData}
        info={cardInfo}
      />
    </div>
  );
}
