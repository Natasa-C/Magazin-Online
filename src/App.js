// Dependencies
import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import "./App.css";

// Components
import Header from "./components/Header";
import MainPage from "./components/MainPage";

// Shared

function App() {
  return (
    <div className="App">
      <Header />
      <MainPage />
    </div>
  );
}

export default App;
