# To install the necessary packages
`npm install`

# To run json-server
`npx json-server --watch src/data/db.json --port 8000`

If the server is started after the react app, the app should be refreshed in order to be able to retrieve the data. 

# To start the React App, in the project directory, you can run:

 ```npm start```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
